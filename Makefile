build:
	docker build -f Dockerfile -t certbot-generator $(shell pwd)
run:
	docker run -v $(shell pwd)/keys:/etc/letsencrypt/archive -it certbot-generator bash
